# Demo Setup for Matweb.info

Updates and starts the demo instance on `https://mat2web.dev`


**IMPORTANT** This is a demo instance do not upload sensitive data!


To start it run the following command:
1) `./start.sh` starts the frontend on `localhost:4000` and the api on `localhost:5000` assuming you have docker and docker-compose installed.

The frontend is configured to use `https://api.mat2web.dev` as api address.

## Resources

Frontend Source: https://0xacab.org/jfriedli/mat2-quasar-frontend
Restful API Source: https://0xacab.org/jvoisin/mat2-web
