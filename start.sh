#!/bin/bash

# remove old seccomp profiles
rm seccomp-*

# download the seccomp rules for the api
curl --request GET -sL \
	     --url 'https://0xacab.org/jvoisin/mat2-web/-/raw/master/config/seccomp.json?inline=false'\
	          --output './seccomp-api.json'

# download the seccomp rules for the frontend
curl --request GET -sL \
	     --url 'https://0xacab.org/jfriedli/mat2-quasar-frontend/-/raw/main/seccomp.json?inline=false'\
	          --output './seccomp-frontend.json'
# get the most current container build
docker-compose pull

# start it
docker-compose up --force-recreate  -d

